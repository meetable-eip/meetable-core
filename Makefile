NAME		= meetable

CC			= g++

RM			= rm -f

OBJ			= $(SRC:.cpp=.o)

SRC			= $(wildcard sources/*.cpp)

##
### Output organized
##

COLOR_OFF	= \033[0m

COLOR_ON	= \033[32;01m

PROG		= $(COLOR_ON) [/!\ Meetable Compiled /!\] $(COLOR_OFF)

##
### FLAGS
##

CXXFLAGS	+= -W -Wall -Werror -Wextra -I./includes

LDFLAGS		+= -lopencv_calib3d -lopencv_core \
			   -lopencv_features2d -lopencv_flann \
			   -lopencv_highgui -lopencv_imgcodecs \
			   -lopencv_imgproc -lopencv_ml \
			   -lopencv_objdetect -lopencv_photo \
			   -lopencv_shape -lopencv_stitching \
			   -lopencv_superres -lopencv_ts \
			   -lopencv_video -lopencv_videoio -lopencv_videostab

##
### System Depedencies
##

UNAME		= $(shell uname -s)

ifeq ($(UNAME),Linux)
    CXXFLAGS += -I/usr/local/opt/opencv3/include
    LDFLAGS  += -L/usr/local/opt/opencv3/lib
endif
ifeq ($(UNAME),Darwin)
    CXXFLAGS += -I/usr/local/opt/opencv3/include
    LDFLAGS  += -L/usr/local/opt/opencv3/lib
endif

##
### RULES
##

all:	$(NAME)

$(NAME): $(OBJ)
		$(CC) $(OBJ) -o $(NAME) $(LDFLAGS)
		@echo "$(PROG)"

clean:
		$(RM) $(OBJ)

fclean: clean
		$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
