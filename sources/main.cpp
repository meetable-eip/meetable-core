#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/imgcodecs/imgcodecs.hpp>
#include <opencv2/videoio/videoio.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace cv;

int                    process(cv::VideoCapture cap) {
    cv::Mat             frame;
    cv::Mat            edges;

    while (cap.isOpened() && (cv::waitKey(30) != 27))
    {
        cap >> frame;
        cv::imshow("video", frame);
    }
    return (0);
}

int                     main() {

    cv::VideoCapture    capture(CV_CAP_ANY);

    try {
        process(capture);
    } catch (cv::Exception &e) {
        std::cerr << "Exception " << e.what() << std::endl;
    }
    return (0);
}
